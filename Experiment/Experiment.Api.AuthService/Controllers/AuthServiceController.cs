﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Experiment.Factory.Interfaces;
using Experiment.Model.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Experiment.Api.AuthService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthServiceController : ControllerBase
    {
        private readonly IAuthFactory _authService;

        public AuthServiceController(IAuthFactory authService)
        {
            this._authService = authService;
        }

        [HttpPost,Route("Register")]
        public async Task<ResultModel<LoginResultModel>> RegisterAsync([FromBody] RegisterParamModel param)
        {
            return await this._authService.RegisterAsync(param);
        }

        [HttpPost, Route("Login")]
        public async Task<ResultModel<LoginResultModel>> Login([FromBody] LoginParamModel param)
        {
            return await this._authService.LoginAsync(param);
        }

        [HttpPost, Route("CreateRole")]
        public async Task<ResultModel<IdentityRole>> CreateRole([FromBody] CreateRoleParamModel param)
        {
            return await this._authService.CreateRole(param);
        }
    }
}
