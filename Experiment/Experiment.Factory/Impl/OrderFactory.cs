﻿using Experiment.DataLayer.Repositories.Interfaces;
using Experiment.Factory.Interfaces;
using Experiment.Model.Entities;
using Experiment.Model.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Experiment.Common.Helpers;
using Newtonsoft.Json;

namespace Experiment.Factory.Impl
{
    public class OrderFactory : IOrderFactory
    {
        private readonly IOrderRepository _orderRepository;
        public OrderFactory(IOrderRepository orderRepository)
        {
            this._orderRepository = orderRepository;
        }

        public async Task<ResultModel<OrderViewModel>> Create(CreateOrderParamModel param)
        {
            ResultModel<OrderViewModel> result = new ResultModel<OrderViewModel>();
            result.Modify("00", "", true, null);
            try
            {
                var dataInsert = new Order()
                {
                    ID = Guid.NewGuid().ToString(),
                    Quantity = param.Quanity,
                    ProductID = param.ProductId,
                    UserID = param.UserId
                };
                var insertRes = this._orderRepository.Create(dataInsert);
                if (!insertRes.IsSuccess)
                {
                    result.Modify("10", $"Created falied : {insertRes.ErrorMessage}", false, null);
                    return result;
                }

                result.Value = new OrderViewModel(dataInsert);

                param.OrderId = dataInsert.ID;

                RabbitMqHelper.Publish("192.168.99.100", "guest", "guest", "Product.CreateOrderExchange", JsonConvert.SerializeObject(param));
            }
            catch (Exception ex)
            {
                result.SetError(ex);
            }

            return result;
        }

        public async Task<ResultModel<OrderViewModel>> RollBackCreate(RollBackOrderParamrModel param)
        {
            ResultModel<OrderViewModel> result = new ResultModel<OrderViewModel>();

            try
            {

                var oldOrder = this._orderRepository.FindById(param.OrderId);

                var deleteRes = this._orderRepository.Delete(oldOrder);

                if (!deleteRes.IsSuccess)
                {
                    RabbitMqHelper.Publish("192.168.99.100", "guest", "guest", "Order.RollbackCreateExchange", JsonConvert.SerializeObject(param));
                }



            }catch(Exception ex)
            {
                result.SetError(ex);
            }

            return result;
        }
    }
}
