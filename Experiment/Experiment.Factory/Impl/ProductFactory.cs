﻿using Experiment.DataLayer.Repositories.Interfaces;
using Experiment.Factory.Interfaces;
using Experiment.Model.Entities;
using Experiment.Model.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Experiment.Common.Helpers;
using Newtonsoft.Json;

namespace Experiment.Factory.Impl
{
    public class ProductFactory : IProductFactory
    {
        private readonly IProductRepository _productRepository;
        public ProductFactory(IProductRepository productRepository)
        {
            this._productRepository = productRepository;
        }

        public async Task<ResultModel<ProductViewModel>> Create(ProductViewModel param)
        {
            ResultModel<ProductViewModel> result = new ResultModel<ProductViewModel>();
            result.Modify("00", "", true, null);
            try
            {
                var dataInsert = new Product()
                {
                    ID = Guid.NewGuid().ToString(),
                    Name = param.Name,
                    CreatedBy = param.CreatedBy,
                    Description = param.Description,
                    Price = param.Price,
                    Stock = param.Stock,
                };
                var insertRes = this._productRepository.Create(dataInsert);
                if (!insertRes.IsSuccess)
                {
                    result.Modify("10", $"Created falied : {insertRes.ErrorMessage}", false, null);
                    return result;
                }

                result.Value = new ProductViewModel(dataInsert);

            }
            catch(Exception ex)
            {
                result.SetError(ex);
            }

            return result;
        }

        public async Task<ResultModel<ProductViewModel>> Delete(string id)
        {
            ResultModel<ProductViewModel> result = new ResultModel<ProductViewModel>();
            result.Modify("00", "", true, null);
            try
            {
                var dataDelete = this._productRepository.FindById(id);
                if(dataDelete == null)
                {
                    result.Modify("11", "Data not found", false, null);
                    return result;
                }

                var deleteRes = this._productRepository.Delete(dataDelete);
                if (!deleteRes.IsSuccess)
                {
                    result.Modify("10", $"Deleted falied : {deleteRes.ErrorMessage}", false, null);
                    return result;
                }
                result.Value = new ProductViewModel(dataDelete);

            }
            catch (Exception ex)
            {
                result.SetError(ex);
            }

            return result;
        }

        public async Task<List<ProductViewModel>> GetAll()
        {
            return await Task.Run(() => { return this._productRepository.GetAll().ToList().Select(x => new ProductViewModel(x)).ToList(); });
        }

        public async Task<ProductViewModel> GetByID(string id)
        {
            return await Task.Run(() => { return new ProductViewModel(this._productRepository.FindById(id)); });
        }

        public async Task<ResultModel<ProductViewModel>> Update(ProductViewModel param)
        {
            ResultModel<ProductViewModel> result = new ResultModel<ProductViewModel>();
            result.Modify("00", "", true, null);
            try
            {
                var dataUpdate = this._productRepository.FindById(param.Id);
                if (dataUpdate == null)
                {
                    result.Modify("11", "Data not found", false, null);
                }

                dataUpdate.Name = param.Name;
                dataUpdate.Price = param.Price;
                dataUpdate.Stock = param.Stock;
                dataUpdate.Description = param.Description;
                dataUpdate.UpdatedBy = param.UpdatedBy;

                var updateRes = this._productRepository.Update(dataUpdate);
                if (!updateRes.IsSuccess)
                {
                    result.Modify("10", $"Deleted falied : {updateRes.ErrorMessage}", false, null);
                    return result;
                }
                result.Value = new ProductViewModel(dataUpdate);

            }
            catch (Exception ex)
            {
                result.SetError(ex);
            }

            return result;
        }

        public async Task<ResultModel<ProductViewModel>> UpdateStock(UpdateStockParamModel param)
        {
            ResultModel<ProductViewModel> result = new ResultModel<ProductViewModel>();
            result.Modify("00", "", true, null);
            try
            {
                var dataUpdate = this._productRepository.FindById(param.Id);
                if (dataUpdate == null)
                {
                    result.Modify("11", "Data not found", false, null);
                    return result;
                }

                if(param.Stock < 0)
                {
                    int reduceStock = Math.Abs(param.Stock);

                    if(reduceStock > dataUpdate.Stock)
                    {
                        result.Modify("12", "Stock not available", false, null);
                        return result;
                    }

                    dataUpdate.Stock -= reduceStock;
                }
                else
                {
                    dataUpdate.Stock = param.Stock;
                }
                

                var updateRes = this._productRepository.Update(dataUpdate);
                if (!updateRes.IsSuccess)
                {
                    result.Modify("10", $"Deleted falied : {updateRes.ErrorMessage}", false, null);
                    return result;
                }
                result.Value = new ProductViewModel(dataUpdate);

            }
            catch (Exception ex)
            {
                result.SetError(ex);
            }

            return result;
        }

        public async Task<ResultModel<ProductViewModel>> CreateOrder(CreateOrderParamModel param)
        {
            ResultModel<ProductViewModel> result = new ResultModel<ProductViewModel>();
            result.Modify("00", "Success", true, null);
            try
            {
                Product product = this._productRepository.FindById(param.ProductId);
                if(product.Stock < param.Quanity)
                {
                    RollBackOrderParamrModel payloadRollback = new RollBackOrderParamrModel
                    {
                        OrderId = param.OrderId,
                        Message = "Out of stock",
                        StatusCode = "10"
                    };

                    RabbitMqHelper.Publish("192.168.99.100", "guest", "guest", "Order.RollbackCreateExchange", JsonConvert.SerializeObject(payloadRollback));
                    result.Modify("12", "Out of stock", false, null);
                    return result;
                }

                //update stock
                UpdateStockParamModel paramUpdate = new UpdateStockParamModel()
                {
                    Id = param.ProductId,
                    Stock = (-1 * param.Quanity)
                };
                var updateRes = await this.UpdateStock(paramUpdate);
                if (!updateRes.Success)
                {
                    RollBackOrderParamrModel payloadRollback = new RollBackOrderParamrModel
                    {
                        OrderId = param.OrderId,
                        Message = "Out of stock",
                        StatusCode = "10"
                    };

                    RabbitMqHelper.Publish("192.168.99.100", "guest", "guest", "Order.RollbackCreateExchange", JsonConvert.SerializeObject(payloadRollback));
                    result.Modify("13", updateRes.StatusMessage, false, null);
                    return result;
                }

            }
            catch (Exception ex)
            {
                result.SetError(ex);
            }

            return result;
        }
    }
}
