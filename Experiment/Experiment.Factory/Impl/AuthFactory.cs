﻿using Experiment.Factory.Interfaces;
using Experiment.Model.Context;
using Experiment.Model.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Options;

namespace Experiment.Factory.Impl
{
    public class AuthFactory : IAuthFactory
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IOptions<ApplicationSetting> _appSetting;
        public AuthFactory(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSetting> appSetting
        )
        {
            this._userManager = userManager;
            this._signInManager = signInManager;
            this._roleManager = roleManager;
            this._appSetting = appSetting;
        }

        public async Task<ResultModel<LoginResultModel>> RegisterAsync(RegisterParamModel param)
        {
            ResultModel<LoginResultModel> result = new ResultModel<LoginResultModel>();
            result.Modify("00", "", true, null);
            try
            {

                ApplicationUser dataUser = new ApplicationUser
                {
                    UserName = param.UserName,
                    Email = param.Email
                };

                var registerRes = await _userManager.CreateAsync(dataUser, param.Password);
                if (!registerRes.Succeeded)
                {
                    var listError = registerRes.Errors.ToList().Select(x => x.Description);
                    string errorStr = string.Join(',', listError);
                    result.Modify("10", $"Register failed : {errorStr}", false, null);
                    return result;
                }

                //var userToVerify = await _userManager.FindByIdAsync(dataUser.Id);

                var setRoleRes = await _userManager.AddToRoleAsync(dataUser, "User");
                if (!setRoleRes.Succeeded)
                {
                    var listError = setRoleRes.Errors.ToList().Select(x => x.Description);
                    string errorStr = string.Join(',', listError);
                    result.Modify("12", $"Register failed : {errorStr}", false, null);
                    return result;
                }

                var identityClaims = (List<Claim>) await _userManager.GetClaimsAsync(dataUser);
                identityClaims.Add(new Claim(ClaimTypes.Name, param.UserName));

                var getRoles = await _userManager.GetRolesAsync(dataUser);
                getRoles.ToList().ForEach(r => {
                    identityClaims.Add(new Claim(ClaimTypes.Role, r));
                });

                var expiresToken = DateTime.Now.AddMinutes(10);

                string tokenStr = _createIdentityToken(
                    identityClaims,
                    this._appSetting.Value.JwtConfig.Key,
                    this._appSetting.Value.JwtConfig.Issuer,
                    this._appSetting.Value.JwtConfig.Issuer,
                    expiresToken
                );

                result.Value = new LoginResultModel()
                {
                    Username = param.UserName,
                    AccessTokenExpiredDate = expiresToken,
                    AccessToken = tokenStr
                };


            }
            catch(Exception ex)
            {
                result.SetError(ex);
            }
            return result;
        }

        public async Task<ResultModel<LoginResultModel>> LoginAsync(LoginParamModel param)
        {
            ResultModel<LoginResultModel> result = new ResultModel<LoginResultModel>();
            result.Modify("00", "", true, null);
            try
            {

                var loginRes = await _signInManager.PasswordSignInAsync(
                    userName: param.UserName,
                    password: param.Password,
                    isPersistent: false,
                    lockoutOnFailure: false
                );

                if (!loginRes.Succeeded)
                {
                    result.Modify("11", $"User not found", false, null);
                    return result;
                }

                var userToverify = await _userManager.FindByNameAsync(param.UserName);
                if (userToverify == null)
                {
                    result.Modify("11", $"User not found", false, null);
                    return result;
                }
                var roles = await _userManager.GetRolesAsync(userToverify);
                //var identityClaims = await _userManager.GetClaimsAsync(userToverify);

                List<Claim> identityClaims = new List<Claim>();
                identityClaims.Add(new Claim(ClaimTypes.NameIdentifier, userToverify.UserName));
                roles.ToList().ForEach(role =>
                {
                    identityClaims.Add(new Claim(ClaimTypes.Role, role));
                });

                identityClaims.Add(new Claim(ClaimTypes.Email, userToverify.Email));

                //create token process
                var expiresToken = DateTime.Now.AddMinutes(10);

                string tokenStr = _createIdentityToken(
                    identityClaims,
                    this._appSetting.Value.JwtConfig.Key,
                    this._appSetting.Value.JwtConfig.Issuer,
                    this._appSetting.Value.JwtConfig.Issuer,
                    expiresToken
                );

                result.Value = new LoginResultModel()
                {
                    Username = param.UserName,
                    AccessTokenExpiredDate = expiresToken,
                    AccessToken = tokenStr
                };
            }
            catch (Exception ex)
            {
                result.SetError(ex);
            }
            return result;
        }

        public async Task<ResultModel<IdentityRole>> CreateRole(CreateRoleParamModel param)
        {
            ResultModel<IdentityRole> result = new ResultModel<IdentityRole>();
            result.Modify("00", "", true, null);
            try
            {

                var dataRole = new IdentityRole()
                {
                    Name = param.Name
                };

                var createRes = await this._roleManager.CreateAsync(dataRole);
                if (!createRes.Succeeded)
                {
                    result.Modify("10", "create new role failed", false, null);
                    return result;
                }

                result.Value = dataRole;

            }
            catch(Exception ex)
            {
                result.SetError(ex);
            }
            return result;
        } 

        private string _createIdentityToken(List<Claim> claims, string key, string issuer, string audience, DateTime expires)
        {
            var keyBytes = Encoding.ASCII.GetBytes(key);
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor() {
                Subject = new ClaimsIdentity(claims),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(keyBytes), SecurityAlgorithms.HmacSha256Signature),
                Audience = audience,
                Issuer = issuer,
                Expires = expires
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
