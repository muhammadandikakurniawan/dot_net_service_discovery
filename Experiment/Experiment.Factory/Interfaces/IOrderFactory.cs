﻿using Experiment.Model.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Experiment.Factory.Interfaces
{
    public interface IOrderFactory
    {
        public Task<ResultModel<OrderViewModel>> Create(CreateOrderParamModel param);
        public Task<ResultModel<OrderViewModel>> RollBackCreate(RollBackOrderParamrModel param);
    }
}
