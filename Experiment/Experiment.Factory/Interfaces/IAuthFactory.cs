﻿using Experiment.Model.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Experiment.Factory.Interfaces
{
    public interface IAuthFactory
    {
        public Task<ResultModel<LoginResultModel>> RegisterAsync(RegisterParamModel param);
        public Task<ResultModel<LoginResultModel>> LoginAsync(LoginParamModel param);
        Task<ResultModel<IdentityRole>> CreateRole(CreateRoleParamModel param);
    }
}
