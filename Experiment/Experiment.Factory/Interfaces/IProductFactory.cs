﻿using Experiment.Model.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Experiment.Factory.Interfaces
{
    public interface IProductFactory
    {
        public Task<ResultModel<ProductViewModel>> Create(ProductViewModel param);
        public Task<ResultModel<ProductViewModel>> Update(ProductViewModel param);
        public Task<ResultModel<ProductViewModel>> Delete(string id);
        public Task<List<ProductViewModel>> GetAll();
        public Task<ProductViewModel> GetByID(string id);
        Task<ResultModel<ProductViewModel>> UpdateStock(UpdateStockParamModel param);
        Task<ResultModel<ProductViewModel>> CreateOrder(CreateOrderParamModel param);
    }
}
