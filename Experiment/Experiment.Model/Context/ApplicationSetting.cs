﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Experiment.Model.Context
{
    public class ApplicationSetting
    {
        public JwtConfigModel JwtConfig { get; set; }
        public RabbitMqConfigModel RabbitMqConfig { get; set; }
    }

    public class JwtConfigModel
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
    }

    public class RabbitMqConfigModel
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
