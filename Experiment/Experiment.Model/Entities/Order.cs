﻿using Experiment.Model.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Experiment.Model.Entities
{
    public class Order : BaseEntity
    {
        [Key]
        public string ID { get; set; }
        public string UserID { get; set; }
        public string ProductID { get; set; }
        public int Quantity { get; set; }


        [ForeignKey("UserID")]
        public ApplicationUser User { get; set; }
        [ForeignKey("ProductID")]
        public Product Product { get; set; }
    }
}
