﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Experiment.Model.Entities
{
    public class BaseEntity
    {
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public bool Isdeleted { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
