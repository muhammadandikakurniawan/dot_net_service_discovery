﻿using Experiment.Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Experiment.Model.Models
{
    public class ProductViewModel
    {
        public ProductViewModel() { }
        public ProductViewModel(Product param)
        {
            this.Id = param.ID;
            this.Name = param.Name;
            this.Stock = param.Stock;
            this.Price = param.Price;
            this.CreatedBy = param.CreatedBy;
            this.Description = param.Description;
        }
        public string Id { get; set; }
        [Required]
        public int Stock { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }

    public class UpdateStockParamModel
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public int Stock { get; set; }
    }
}
