﻿using Experiment.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Experiment.Model.Models
{
    public class OrderViewModel{
        public string ProductId { get; set; }
        public int Quanity { get; set; }

        public OrderViewModel() { }

        public OrderViewModel(Order param)
        {
            this.ProductId = param.ProductID;
        }
    }
    public class CreateOrderParamModel
    {
        public string OrderId { get; set; }
        public string UserId { get; set; }
        public string ProductId { get; set; }
        public int Quanity { get; set; }
    }

    public class RollBackOrderParamrModel
    {
        public string OrderId { get; set; }
        public string Message { get; set; }
        public string StatusCode { get; set; }
    }
}
