﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Experiment.Model.Models
{
    public class LoginParamModel
    {
        [Required]
        public string UserName { get;set; }
        [Required]
        public string Password { get; set; }
    }
    public class RegisterParamModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        [Required, Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
    public class LoginResultModel
    {
        public string Username { get; set; }
        public string AccessToken { get; set; }
        public DateTime AccessTokenExpiredDate { get; set; }
    }
    public class CreateRoleParamModel
    {
        public string Name { get; set; }
    }
}
