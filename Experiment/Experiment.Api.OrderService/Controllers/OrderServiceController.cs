﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Experiment.Factory.Interfaces;
using Experiment.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Experiment.Api.OrderService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderServiceController : ControllerBase
    {
        private readonly IOrderFactory _orderFactory;
        public OrderServiceController(IOrderFactory orderFactory)
        {
            this._orderFactory = orderFactory;
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] CreateOrderParamModel param)
        {
            var result = await this._orderFactory.Create(param);
            return Ok(result);
        }

        [HttpPost]
        [Route("RollBackCreate")]
        public async Task<IActionResult> RollBackCreate([FromBody] RollBackOrderParamrModel param)
        {
            var result = await this._orderFactory.RollBackCreate(param);
            return Ok(result);
        }
    }
}
