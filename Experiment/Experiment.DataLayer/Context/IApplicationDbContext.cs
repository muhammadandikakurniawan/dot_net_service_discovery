﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;

namespace Experiment.DataLayer.Context
{
    public interface IApplicationDbContext
    {
        int SaveChanges();
        DbSet<T> Set<T>() where T : class;
        EntityEntry Entry(object o);

        void Dispose();
    }
}
