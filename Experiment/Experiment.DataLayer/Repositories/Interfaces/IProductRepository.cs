﻿using Experiment.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Experiment.DataLayer.Repositories.Interfaces
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        Product FindById(string id);
    }
}
