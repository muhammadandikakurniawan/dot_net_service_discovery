﻿using Experiment.Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Experiment.DataLayer.Repositories.Interfaces
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        Order FindById(string id);
    }
}
