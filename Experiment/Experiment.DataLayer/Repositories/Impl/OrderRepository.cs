﻿using Experiment.DataLayer.Context;
using Experiment.DataLayer.Repositories.Interfaces;
using Experiment.Model.Entities;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Experiment.DataLayer.Repositories.Impl
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(IApplicationDbContext dbContext) : base(dbContext)
        {

        }

        public Order FindById(string id)
        {
            return this.GetAll().FirstOrDefault(x => x.ID == id);
        }
    }
}
