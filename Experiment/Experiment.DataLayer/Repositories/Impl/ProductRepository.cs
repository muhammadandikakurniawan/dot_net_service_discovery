﻿using Experiment.DataLayer.Context;
using Experiment.DataLayer.Repositories.Interfaces;
using Experiment.Model.Entities;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Experiment.DataLayer.Repositories.Impl
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(IApplicationDbContext dbContext) : base(dbContext)
        {

        }

        public Product FindById(string id)
        {
            return this.GetAll().FirstOrDefault(x => x.ID == id);
        }
    }
}
