﻿using Experiment.DataLayer.Context;
using Experiment.Model.Entities;
using Experiment.Model.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Experiment.DataLayer.Repositories
{
    public class BaseRepository<T> : IDisposable, IBaseRepository<T> where T : BaseEntity
    {
        protected IApplicationDbContext _dbContext;
        protected DbSet<T> _dbSet;
        public BaseRepository(IApplicationDbContext dbContext)
        {
            this._dbContext = dbContext;
            this._dbSet = this._dbContext.Set<T>();
        }

        public EFResultModel<T> Create(T param)
        {
            EFResultModel<T> result = new EFResultModel<T>();
            try
            {
                param.CreatedDate = DateTime.Now;

                this._dbSet.Add(param);
                this._dbContext.SaveChanges();

            }catch(Exception ex)
            {
                result.IsSuccess = false;
                result.ErrorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            return result;
        }

        public EFResultModel<T> Update(T param)
        {
            EFResultModel<T> result = new EFResultModel<T>();
            try
            {
                param.UpdatedDate = DateTime.Now;
                this._dbContext.Entry(param).State = EntityState.Modified;
                this._dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.ErrorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            return result;
        }

        public EFResultModel<T> Delete(T param)
        {
            EFResultModel<T> result = new EFResultModel<T>();
            try
            {
                if(this._dbContext.Entry(param).State == EntityState.Detached)
                {
                    this._dbSet.Attach(param);
                }
                this._dbSet.Remove(param);
                this._dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.ErrorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            return result;
        }

        public EFResultModel<T> SoftDelete(T param)
        {
            EFResultModel<T> result = new EFResultModel<T>();
            try
            {
                param.UpdatedDate = DateTime.Now;
                param.Isdeleted = true;
                this.Update(param);
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.ErrorMessage = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }

            return result;
        }

        public IQueryable<T> GetAll()
        {
            return this._dbSet;
        }

        public void Dispose()
        {
            if(_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
