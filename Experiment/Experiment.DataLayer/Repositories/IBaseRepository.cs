﻿using Experiment.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Experiment.DataLayer.Repositories
{
    public interface IBaseRepository<T>
    {
        public EFResultModel<T> Create(T param);
        public EFResultModel<T> Update(T param);
        public EFResultModel<T> Delete(T param);
        public EFResultModel<T> SoftDelete(T param);
        public IQueryable<T> GetAll();
    }
}
