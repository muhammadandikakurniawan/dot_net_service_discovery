﻿using Experiment.Common.Helpers;
using Experiment.Model.Models;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Experiment.Consumer.Consumers
{
    public static class OrderConsumer
    {

        public static void ProductCreateOrder()
        {
            var factory = new ConnectionFactory()
            {
                HostName = "192.168.99.100",
                UserName = "guest",
                Password = "guest",
            };

            string exchange = "Product.CreateOrderExchange";
            string queue = "Product.CreateOrderQueue";

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {

                    channel.ExchangeDeclare(exchange: exchange, type: ExchangeType.Fanout);

                    channel.QueueDeclare(queue: queue, exclusive: false);

                    channel.QueueBind(queue: queue, exchange: exchange, routingKey: "");

                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);

                        string url = "https://localhost:5003/ProductService/CreateOrder";
                        CreateOrderParamModel paramCreateOrder = JsonConvert.DeserializeObject<CreateOrderParamModel>(message);
                        var res = HttpHelper.PostAsync<ResultModel<ProductViewModel>>(url, body: paramCreateOrder).Result;

                        Console.WriteLine($"{DateTime.Now.ToShortDateString()} | {exchange} | {message} ");
                    };

                    channel.BasicConsume(queue: queue, autoAck: true, consumer: consumer);

                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                }
            }
        }

        public static void OrderRollbackCreate()
        {
            var factory = new ConnectionFactory()
            {
                HostName = "192.168.99.100",
                UserName = "guest",
                Password = "guest",
            };

            string exchange = "Order.RollbackCreateExchange";
            string queue = "Order.RollbackCreateOrderQueue";

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {

                    channel.ExchangeDeclare(exchange: exchange, type: ExchangeType.Fanout);

                    channel.QueueDeclare(queue: queue, exclusive: false);

                    channel.QueueBind(queue: queue, exchange: exchange, routingKey: "");

                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);

                        string url = "https://localhost:5003/OrderService/RollBackCreate";
                        RollBackOrderParamrModel paramCreateOrder = JsonConvert.DeserializeObject<RollBackOrderParamrModel>(message);
                        var res = HttpHelper.PostAsync<ResultModel<ProductViewModel>>(url, body: paramCreateOrder).Result;

                        Console.WriteLine($"{DateTime.Now.ToShortDateString()} | {exchange} | {message} ");
                    };

                    channel.BasicConsume(queue: queue, autoAck: true, consumer: consumer);

                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                }
            }
        }

    }
}
