﻿using Experiment.Consumer.Consumers;
using System;
using System.Threading.Tasks;

namespace Experiment.Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(() => { OrderConsumer.ProductCreateOrder(); });
            OrderConsumer.OrderRollbackCreate();
            Console.WriteLine("Hello World!");
        }
    }
}
