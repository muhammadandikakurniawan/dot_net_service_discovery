﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Experiment.Factory.Interfaces;
using Experiment.Model.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Experiment.Api.ProductService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductServiceController : ControllerBase
    {

        private readonly IProductFactory _productFactory;
        public ProductServiceController(IProductFactory productFactory)
        {
            this._productFactory = productFactory;
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] ProductViewModel param)
        {
            var result = await this._productFactory.Create(param);
            return Ok(result);
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] ProductViewModel param)
        {
            var result = await this._productFactory.Update(param);
            return Ok(result);
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete([FromBody]  Dictionary<string,string> param)
        {
            string Id = null;
            if(!param.TryGetValue("Id",out Id)){
                return BadRequest();
            }
            var result = await this._productFactory.Delete(Id);
            return Ok(result);
        }

        [HttpPost]
        [Route("GetByID")]
        public async Task<IActionResult> GetByID([FromBody] Dictionary<string, string> param)
        {
            string Id = null;
            if (!param.TryGetValue("Id", out Id))
            {
                return BadRequest();
            }
            var result = await this._productFactory.GetByID(Id);
            return Ok(result);
        }

        [HttpPost]
        [Route("UpdateStock")]
        public async Task<IActionResult> UpdateStock([FromBody] UpdateStockParamModel param)
        {
            var result = await this._productFactory.UpdateStock(param);
            return Ok(result);
        }

        [HttpPost]
        [Route("CreateOrder")]
        public async Task<IActionResult> CreateOrder([FromBody] CreateOrderParamModel param)
        {
            var result = await this._productFactory.CreateOrder(param);
            return Ok(result);
        }

    }
}
