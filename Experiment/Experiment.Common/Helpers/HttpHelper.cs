﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Experiment.Common.Helpers
{
    public static class HttpHelper
    {
        public static async Task<T> PostAsync<T>(string url, object body = null, string auth = null, Dictionary<string,string> headers = null)
        {
            T result = default;

            HttpClient httpClient = new HttpClient();
            
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url);

            if (!string.IsNullOrEmpty(auth))
            {
                request.Headers.Add("Authorization", auth);
            }
            if(headers != null)
            {
                headers.Keys.ToList().ForEach(x =>
                {
                    request.Headers.Add(x, headers[x]);
                });
            }
            if(body != null)
            {
                request.Content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            }

            var res = await httpClient.SendAsync(request);

            string stringContent = await res.Content.ReadAsStringAsync();

            result = JsonConvert.DeserializeObject<T>(stringContent);

            return result;
        }

        public static async Task<T> GetAsync<T>(string url, string auth = null, Dictionary<string, string> headers = null)
        {
            T result = default;

            HttpClient httpClient = new HttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url);

            if (!string.IsNullOrEmpty(auth))
            {
                request.Headers.Add("Authorization", auth);
            }
            if (headers != null)
            {
                headers.Keys.ToList().ForEach(x =>
                {
                    request.Headers.Add(x, headers[x]);
                });
            }

            var res = await httpClient.SendAsync(request);

            string stringContent = await res.Content.ReadAsStringAsync();

            result = JsonConvert.DeserializeObject<T>(stringContent);

            return result;
        }
    }
}
